//
//  SearchViewController.m
//  BiteBC
//
//  Created by brst on 7/10/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "SearchViewController.h"
#import "DetailPageTableViewController.h"
#import "AppDelegate.h"

@interface SearchViewController ()
{
    
    UIView* bckV;
     UIView* bckV1;
    UIButton *button,*button2;
    
    NSMutableArray* myArray,*dataArray;
}

@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
    
    
    [self fetchRestaurent];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    }
-(void)loadView
{
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];
    
    self.navigationItem.title=@"Search";

    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;
    
}

- (void)viewDidLoad
{
    self.view.backgroundColor=[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];
  
    myArray=[[NSMutableArray alloc]initWithObjects:@"CUISINE",@"OFFER TYPE",@"NO OF PEOPLE",nil];
    dataArray=[[NSMutableArray alloc]init];
    [super viewDidLoad];
    //SEARCHBAR HERE
    self.search=[[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 320, 35)];
    self.search.searchBarStyle=UISearchBarStyleMinimal;
   self.search.placeholder=@"search";
   // self.search.tintColor=[UIColor whiteColor];
    //self.search.backgroundColor=[UIColor  colorWithRed:0.2863 green:0.3216 blue:0.3490 alpha:1];
    self.search.backgroundColor=[UIColor whiteColor];
    self.search.delegate=self;
    
    for (UIView *subView in self.search.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UIColor * color=[UIColor lightGrayColor];
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                
                //set font color here
                searchBarTextField.textColor = [UIColor blackColor];
                searchBarTextField.attributedPlaceholder=[[NSAttributedString alloc] initWithString:@"Search by Restaurant" attributes:@{NSForegroundColorAttributeName: color}];
                
                break;
            }
        }
    }

       [self.view addSubview:self.search];
    
   
    

}

#pragma mark -UISearchBar Delegates

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if ([searchText length] == 0)
    {
        
        [searchBar performSelector:@selector(resignFirstResponder)withObject:nil afterDelay:0];
        
        
        
    }
    
    if (![searchText isEqualToString:@""]) // here you check that the search is not null, if you want add another check to avoid searches when the characters are less than 3
    {
        
        
        searchString=searchText;
        
        NSLog(@"%@",searchString);
        
    }
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
         
    [searchBar resignFirstResponder];
    //160022
//    [self postZipcode];
    
}

#pragma mark -Post Methods
//-(void)postZipcode
//{
//    
//    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
//    
//    NSString *post = [NSString stringWithFormat:@"datasearch=%@",searchString];
//    
//    //    NSMutableDictionary *testDict = [[NSMutableDictionary alloc]init];
//    //    [testDict setObject:[NSArray arrayWithObjects:@"Italian",nil] forKey:@"cuisine_types"];
//    //    [testDict setObject:[NSArray arrayWithObjects:@"50% off food bill",nil] forKey:@"offer_type"];
//    //    [testDict setObject:@"1" forKey:@"people"];
//    
//    
//    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//    
//    NSString *urlString=[NSString stringWithFormat:@"%@product.php?",post_url];
//    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
//    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
//    
//    [request setTimeoutInterval:180];
//    [request setHTTPMethod:@"POST"];
//    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//    [request setHTTPBody:postData];
//    
//    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
//    
//    
//    
//}
-(void)fetchRestaurent
{
    
    
    //http://bitebc.ca/api/api/products.php
    NSString* strURL=[NSString stringWithFormat:@"%@products.php",post_url];
    NSURL *url3=[NSURL URLWithString:strURL];
    NSURLRequest *request=[NSURLRequest requestWithURL:url3];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    connection=[[NSURLConnection alloc]initWithRequest:request delegate:self];
    
    
    
}
#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    datalDict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"JSON=%@",datalDict);
    
    
    

}

#pragma mark - Uiconnection delegates
//- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
//{
//    data=[[NSMutableData alloc]init];
//}
//- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
//{
//    [data appendData:theData];
//}
//
//- (void)connectionDidFinishLoading:(NSURLConnection *)connection
//{
//    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
//    searchDict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
//    NSLog(@"data is = \n %@",searchDict);
//    
//    detailDict=[searchDict valueForKeyPath:@"product1.detail"];
//   
//
//    cuisineStr=[detailDict valueForKey:@"cuisine"];
//    NSArray* name=[cuisineStr  componentsSeparatedByString:@","];
//    NSLog(@"cuisne =\n %@",name);
//
//    offerAvlStr=[detailDict valueForKey:@"offer_available"];
//    noOfPeopleStr=[detailDict valueForKey:@"number_people"];
//    
//    [dataArray addObject:name];
//    //[cu addObject:@[name[0]]];
//    // [cu addObject:@[@" Asian",@" French",@" British"]];
//    
//    if ([offerAvlStr isEqual:[NSNull null]])
//    {
//        [dataArray addObject:@[@" N.A"]];
//    }
//    else
//    {
//        [dataArray addObject:@[ offerAvlStr]];
//    }
//    
//    if ([noOfPeopleStr isEqual:[NSNull null]])
//    {
//        [dataArray addObject:@[@" 0"]];
//    }else
//    {
//        [dataArray addObject:@[ noOfPeopleStr]];
//    }
//    
//     [self buttonsView];
//    
//
//    
//}
-(void)buttonsView
{
    int h=0;
    searchView=[[UIView alloc]initWithFrame:CGRectMake(0, 40, 320, 380)];
    searchView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:searchView];
    
    
    for (int i=0; i<myArray.count; i++)
    {
        button=[UIButton buttonWithType: UIButtonTypeSystem];
        button.backgroundColor= [UIColor colorWithRed:0.0000 green:0.4667 blue:0.4196 alpha:1.0];
        button.frame=CGRectMake(0, h,320,30);
        [button setTitle:[myArray objectAtIndex:i] forState:UIControlStateNormal];
        button.tag=i;
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        button.titleLabel.font=[UIFont fontWithName:@"Helvetica Neue" size:14];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        button.contentEdgeInsets=UIEdgeInsetsMake(2, 5, 3, 3);
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [searchView addSubview:button];
        NSArray *arr1=[dataArray objectAtIndex:i];
        h=h+button.frame.size.height+1;
        
        for (int j=0; j<arr1.count; j++)
        {
            UILabel* value=[[UILabel alloc]initWithFrame:CGRectMake(0, h, 320, 18)];
            value.text=arr1[j];
            value.textColor=[UIColor blackColor];
            value.backgroundColor=[UIColor whiteColor];
            value.font=[UIFont systemFontOfSize:14];
            h=h+value.frame.size.height+1;
            [searchView addSubview:value];
            
            
        }
        
        
        
        //        UIImageView* imageView=[[UIImageView alloc]initWithFrame:CGRectMake(290,7, 17,17)];
        //        imageView.layer.masksToBounds=YES;
        //        imageView.image=[UIImage imageNamed:@"checkin.png"];
        //        h+=33;
        //
        //        [button addSubview:imageView];
        
        
        
        
    }
    
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}


-(void)buttonPressed:(UIButton *)sender
{
    if (sender.tag==0)
    {
        NSLog(@"%ld",(long)sender.tag);
        
        
    }
    else if (sender.tag==1)
    {
        NSLog(@"%ld",(long)sender.tag);
    }
    else if (sender.tag==2)
    {
        NSLog(@"%ld",(long)sender.tag);
    }
    else if (sender.tag==3)
    {
        NSLog(@"%ld",(long)sender.tag);
    }
//    DetailPageTableViewController * detailPage=[[DetailPageTableViewController alloc]init];
//    detailPage.idString=[NSString stringWithFormat:@"%ld",(long)sender.tag];
//    detailPage.pageTitle=[myArray objectAtIndex:sender.tag];
//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//
//    [self.navigationController pushViewController:detailPage animated:YES];
    
}
//-(void)buttonPressed2
//{
//    if (value==1)
//    {
//        button2.frame=CGRectMake(0,32 ,320,30);
//        [bckV removeFromSuperview];
//        value=0;
//        
//    }
//   // [self customViews2];
//    
//}
//
//-(void)customViews
//{
//    
//      bckV=[[UIView alloc]initWithFrame:CGRectMake(0, 30, 320, 105)];
//    bckV.backgroundColor=[UIColor clearColor];
//    
//    [searchView addSubview:bckV];
//    int h=0;
//    for (int i=0; i<5; i++)
//    {
//       UIView*  listViews=[[UIView alloc]initWithFrame:CGRectMake(0, h, 320, 20)];
//        listViews.backgroundColor=[UIColor whiteColor];
//        listViews.tag=i;
//        h+=21;
//        [bckV addSubview:listViews];
//
//    }
//    
//    
//}
//-(void)customViews2
//{
//    
//    bckV1=[[UIView alloc]initWithFrame:CGRectMake(0, 170, 320, 105)];
//    bckV1.backgroundColor=[UIColor clearColor];
//    [searchView addSubview:bckV1];
//    int h=0;
//    for (int i=0; i<2; i++)
//    {
//        UIView*  listViews=[[UIView alloc]initWithFrame:CGRectMake(0, h, 320, 20)];
//        listViews.backgroundColor=[UIColor whiteColor];
//        listViews.tag=i;
//        h+=21;
//        [bckV1 addSubview:listViews];
//        
//    }
//    
//    
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
