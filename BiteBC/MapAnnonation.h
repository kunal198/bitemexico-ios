//
//  MapAnnonation.h
//  BiteBC
//
//  Created by brst on 8/6/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapAnnonation : NSObject<MKAnnotation>
{
    
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
    NSString *adress;
    NSString *imageurl;
    NSString *cuisine;
    
    
}
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *adress;
@property (nonatomic, copy) NSString *imageurl;
@property (nonatomic, copy) NSString *id_str;
@property (nonatomic, copy) NSString *cuisine;


@end

