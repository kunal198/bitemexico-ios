//
//  SliderPageViewController.m
//  BiteBC
//
//  Created by brst on 11/7/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "SliderPageViewController.h"
#import "ImageViewController.h"
#import "AppDelegate.h"

@interface SliderPageViewController ()
{
}

@end

@implementation SliderPageViewController



 -(void)viewWillAppear:(BOOL)animated
{
        self.navigationController.navigationBarHidden=NO;
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
       self.title=@"Get Start";
    
       self.title=@"How it works";
    
}





-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
        self.view = view;
    
}






- (void)viewDidLoad
{
    [super viewDidLoad];
 
    _modelArray = [NSMutableArray arrayWithObjects:[[ImageModel alloc] initWithImageName:@"help1"],
                   [[ImageModel alloc] initWithImageName:@"help2"],
                   [[ImageModel alloc] initWithImageName:@"help3"],
                   [[ImageModel alloc] initWithImageName:@"help4"],nil];
    

    
    _pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                          navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                        options:[NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:50.0f] forKey:UIPageViewControllerOptionInterPageSpacingKey]];
    
    _pageViewController.delegate = self;
    _pageViewController.dataSource = self;
    
    
    ImageViewController *imageViewController = [[ImageViewController alloc] init];
    imageViewController.model = [_modelArray objectAtIndex:0];
    NSArray *viewControllers = [NSArray arrayWithObject:imageViewController];
    
    [self.pageViewController setViewControllers:viewControllers
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:NO
                                     completion:nil];
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    
    [_pageViewController didMoveToParentViewController:self];
    
    CGRect pageViewRect = self.view.bounds;
    pageViewRect = CGRectInset(pageViewRect, 0.0, 0.0f);
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    self.view.gestureRecognizers = _pageViewController.gestureRecognizers;
    
//    _rateStepper = [[UIStepper alloc] initWithFrame:CGRectMake(40, 440, 40, 30)];
//    [_rateStepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventValueChanged];
//    [_rateStepper setMinimumValue:0];
//    [_rateStepper setMaximumValue:10];
//    [_rateStepper setIncrementImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//    [_rateStepper setDecrementImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//    [[self view] addSubview:_rateStepper];
//    
//    _imageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
//    _imageLabel.backgroundColor = [UIColor clearColor];
//    _imageLabel.textColor = [UIColor whiteColor];
//    [_imageLabel setFont:[UIFont boldSystemFontOfSize:20]];
//    [_imageLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
//    [_imageLabel setTextAlignment:NSTextAlignmentCenter];
//    ImageModel *model = [_modelArray objectAtIndex:0];
//    
//
//    _imageLabel.text = [NSString stringWithFormat:@"%@ - Rating: %ld", model.imageName, (long)model.rating];
//    [[self view] addSubview:_imageLabel];
//
    
}







- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    ImageViewController *contentVc = (ImageViewController *)viewController;
    
    NSUInteger currentIndex = [_modelArray indexOfObject:[contentVc model]];
    _vcIndex = currentIndex;
    [self tittle:currentIndex];
    
    [_rateStepper setValue:[[contentVc model] rating]];
    ImageModel *model = [_modelArray objectAtIndex:_vcIndex];
    [_imageLabel setText:[NSString stringWithFormat:@"%@ - Rating: %ld", model.imageName, (long)model.rating]];
    
    if (currentIndex == 0)
    {
        return nil;
    }
    
    ImageViewController *imageViewController = [[ImageViewController alloc] init];
    imageViewController.model = [_modelArray objectAtIndex:currentIndex - 1];
    return imageViewController;
}







- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    ImageViewController *contentVc = (ImageViewController *)viewController;
    
    NSUInteger currentIndex = [_modelArray indexOfObject:[contentVc model]];
    _vcIndex = currentIndex;
     [self tittle:currentIndex];
    [_rateStepper setValue:[[contentVc model] rating]];
    ImageModel *model = [_modelArray objectAtIndex:_vcIndex];
    [_imageLabel setText:[NSString stringWithFormat:@"%@ - Rating: %ld", model.imageName, (long)model.rating]];
    
    if (currentIndex == _modelArray.count - 1)
    {
        return nil;
    }
    
    
    
    ImageViewController *imageViewController = [[ImageViewController alloc] init];
    imageViewController.model = [_modelArray objectAtIndex:currentIndex + 1];
    return imageViewController;
    
}





-(void)tittle:(NSUInteger)index
{
    NSLog(@"%lu",(unsigned long)index);
    if (index==0)
    {
        self.title=@"How it works";
      
    }else if (index==1)
    {
        self.title=@"Offers";
        
    }else if (index==2)
    {
        self.title=@"Checkin";
       
    }else if (index==3)
    {
        self.title=@"Membership";
        
        UIBarButtonItem * item=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneBtnPress)];
        self.navigationItem.rightBarButtonItem=item;
    }
}



-(void)doneBtnPress
{
    [[NSUserDefaults standardUserDefaults]setValue:@"success" forKey:@"getstart"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    

    [self.navigationController popToRootViewControllerAnimated:YES];
}



- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return _modelArray.count;
}



- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}



- (void)stepperValueChanged:(id)sender
{
    
    
    ImageModel *model = [_modelArray objectAtIndex:_vcIndex];
    [model setRating:[_rateStepper value]];
    [_imageLabel setText:[NSString stringWithFormat:@"%@ - Rating: %ld", model.imageName, (long)model.rating]];
}




 - (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
