//
//  SliderPageViewController.h
//  BiteBC
//
//  Created by brst on 11/7/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SliderPageViewController : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate>
@property (nonatomic, strong) UIPageViewController *pageViewController;
@property (nonatomic, strong) NSMutableArray *modelArray;
@property (nonatomic) NSInteger vcIndex;
@property (nonatomic, strong) UIStepper *rateStepper;
@property (nonatomic, strong) UILabel *imageLabel;

- (void)stepperValueChanged:(id)sender;

@end
