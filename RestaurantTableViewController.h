//
//  RestaurantTableViewController.h
//  BiteBC
//
//  Created by brst on 7/4/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"

@interface RestaurantTableViewController : UITableViewController<UISearchBarDelegate,UISearchDisplayDelegate,NSURLConnectionDelegate,UIAlertViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UIScrollViewDelegate>

{
    UIView *blurView;
   
    
    NSMutableArray *hotelList,*cusineListArray,*discountList,*photoArray;
    
      NSMutableArray *hotelNameA,*cuisineArray,*idArray,*addressArray,*ratingArray;
    
    UIView *cellView;
    
    UIImageView* profileImageView,*discountImage;
    
    
    UILabel *hotelName_Label,*cusine_label,*savedLabel,*disAmountLabel;
   
    ASStarRatingView *staticStarRatingView;
    
    UIBarButtonItem* barButton;
    UIButton* filterButton;
    
    
    UILabel *firstLabel,*secondLabel,*thirdLabel,*fourthLabel;
    
    NSURLConnection* connection;
    
    NSMutableDictionary* dict;
    
    NSMutableData* data;
    
    NSMutableArray *dataArray;
    
    NSMutableArray *arrayOfAllData;
    
    UIBarButtonItem *refreshButton;
    
    NSMutableArray *cityArray;
    NSMutableArray *city_idArray;
    UIPickerView *pickerViewData;
    UITextField *txtCity;
    UILabel *lblAlert;
    NSString *checkCity;
    UIToolbar *toolBarForPeriodPicker;
    NSString *cityID;
    UITouch *touch;
    NSString *cityName;
    NSString *cityid;
   
}
    

 @property(strong,nonatomic)UISearchBar* searchBar;

@end
