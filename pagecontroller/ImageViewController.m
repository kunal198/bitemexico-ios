//
//  ImageViewController.m
//  PageController
//
//  Created by brst on 11/7/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController ()

@end

@implementation ImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    CGRect insetFrame = CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
    _imageView = [[UIImageView alloc] initWithFrame:insetFrame];
    _imageView.backgroundColor = [UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];
    _imageView.layer.masksToBounds=YES;
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    [_imageView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [_imageView setImage:[UIImage imageNamed:_model.imageName]];
    [[self view] addSubview:_imageView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
